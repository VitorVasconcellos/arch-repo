# Arch Repository

This branch contains the repo itself and all package files.

## How to use

Just add a section for this repository in `/etc/pacman.conf`.

Adding this repository before Arch's core ones will allow it to override some
official packages with custom build one. If this is not a desired behaviour add
the repository after.

```ini
[vasconcellos]
SigLevel = Optional
Server = https://gitlab.com/VitorVasconcellos/arch-repo/-/raw/repo/$repo/$arch
```
